let mapleader=','
" I want ':E' to always mean ':Explore', and not e.g.
" ':EditorConfigEnable'
cabbrev E Explore

" Normal maps
nnoremap <F2> :call QuickCompile()<CR>
nnoremap <F3> :call RunLinter()<CR>
nnoremap <F8> :bufdo bd<CR>
"noremap <F3> :execute ":!git log -L " . (line('.')) . "," . line('.') . ":" . "%"<CR>
noremap <F4> :execute ":!git blame -L " . (line('.')+10) . "," . line('.') . " " . "%"<CR>
nnoremap <C-K> :Ack! ''<left>
nnoremap <C-]> :execute "tjump " . expand("<cword>")<CR>
nnoremap <C-J> :execute "ptag " . expand("<cword>")<CR>
nnoremap <C-N> :CtrlPBufTag<CR>
nnoremap [g :cn<CR>
nnoremap ]g :cp<CR>

" Insert maps
inoremap ,, =>
