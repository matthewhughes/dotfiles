function! QuickCompile()
    silent make
    if !empty(getqflist())
        exec 'botright copen'
        nnoremap <buffer> q :close<CR>
    endif
    redraw!
endfunction
