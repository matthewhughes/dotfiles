function! RunLinter()
    if !exists('b:linter')
        echoerr "RunLinter: b:linter not set"
        return 1
    endif
    if exists("b:current_compiler")
        let s:compiler = b:current_compiler
    endif

    execute ':compiler ' . b:linter
    silent make

    " Restore the compiler
    if exists("s:compiler")
        execute ':compiler ' . s:compiler
    endif

    " display results
    if !empty(getqflist())
        exec 'botright copen'
        nnoremap <buffer> q :close<CR>
    endif
    redraw!
endfunction
