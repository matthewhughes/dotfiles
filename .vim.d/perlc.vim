function! Perlcritic()
    compiler perlcritic
    silent make
    exec 'botright copen'
    nnoremap <buffer> q :close<CR>
    redraw!
endfunction
