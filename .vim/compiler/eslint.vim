" Vim Compiler File
" Compiler: eslint
" Last Change: 2018 Nov 15 

if exists("current_compiler")
	finish
endif
let current_compiler = "eslint"

if exists(":CompilerSet") != 2
	command -nargs=* CompilerSet setlocal <args>
endif

let s:cpo_save = &cpo
set cpo-=C

CompilerSet makeprg=$(npm\ bin)/eslint\ -f\ unix\ %
CompilerSet errorformat=%f:%l:%c:%m,%-G%.%#

let &cpo = s:cpo_save
unlet s:cpo_save
