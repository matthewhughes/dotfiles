" Vim compiler file
" Compiler:             pandoc via a supported pdf engine
" Maintainer:           Matthew Hughes <matthewhughes934@gmail.com>
" Latest Revision       2019-02-03
" Requires 'pandoc' and an engine for creating PDFs,
" see 'Creating a PDF' in the pandoc manual

if exists("current_compiler")
    finish
endif
let current_compiler = "pandoc_pdf"

if exists(":CompilerSet") != 2
    command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet errorformat&    " use the default 'errorformat'
CompilerSet makeprg=pandoc\ %\ -o\ %<.pdf
