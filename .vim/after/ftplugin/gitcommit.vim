setlocal tabstop=4
setlocal shiftwidth=4
setlocal nocindent
au FileType gitcommit set tw=72
