setlocal noexpandtab
setlocal shiftwidth=4
setlocal tabstop=4
setlocal expandtab
setlocal textwidth=79
setlocal nocindent
compiler pandoc_pdf
nnoremap <buffer> <leader>lv :silent :execute ':!mupdf ' . '%<' . '.pdf' . ' &'<CR>
