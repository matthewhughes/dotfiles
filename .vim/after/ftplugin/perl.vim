setlocal keywordprg=perldoc
setlocal formatprg=perltidy
let b:linter = 'perlcritic'

function! GetPrevSub()
    let [s:lnum, s:col] = searchpos('\%\(^sub\%\(test\)\? \)\@<=[a-z_]\+\c', 'bcn')
    return s:lnum ? getline(s:lnum)[s:col -1:] : ''
endfunction

nnoremap <F5> :execute ":Git blame -L :" . GetPrevSub(). " " . "%"<CR>
nnoremap <leader>a :call search('^sub\%\(test\)\? [a-z_-]\+\c', 'b')<CR>
