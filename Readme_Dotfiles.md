# Dotfiles

Some extra information about configurations not tracked in this repo.

## DPMS Config

```
$ cat /etc/X11/xorg.conf.d/10-monitor.conf
Section "ServerLayout"
    Identifier "ServerLayout0"
    Option "StandbyTime" "11"
    Option "SuspendTime" "20"
    Option "OffTime"     "30"
    Option "BlankTime"   "40"
EndSection
```

## Keyboard Config

```
$ cat /etc/default/keyboard
# KEYBOARD CONFIGURATION FILE

# Consult the keyboard(5) manual page.

XKBMODEL="pc105"
XKBLAYOUT="us"
XKBVARIANT=""
XKBOPTIONS="caps:swapescape"

BACKSPACE="guess"
```
