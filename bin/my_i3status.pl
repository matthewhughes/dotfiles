#!/usr/bin/env perl

use strict;
use warnings;

use feature qw(say state);
use JSON;

# don't buffer any output
$| = 1;

# skip first line, contains the version header
print scalar <STDIN>;

# The second line contains the start of the infinite array
print scalar <STDIN>;

# read, manipulate, and write the line
while (my ($statusline) = (<STDIN> =~ /^,?(.*)/)) {
	my $blocks = decode_json($statusline);

	my %weather_info = (
		full_text => get_weather() . ' ',
		name      => 'the-current-weather',
	);
	unshift @$blocks, \%weather_info;

	say encode_json($blocks) . ",";
}

sub get_weather
{
	state $weather;
	my $this_minute = [localtime]->[1];

	if ($this_minute % 10 == 0 || !$weather) {
		$weather = `i3status_weather --city 'Balham' --open-weather-key $ENV{OPENWEATHER_KEY}`;
	}
	return $weather;
}
