#!/bin/bash

i3-save-tree --workspace $1  | tail -n +2 | fgrep -v '// splitv' | fgrep -v '// splith' | sed 's|//||g' > ~/.config/i3/workspaces/workspace-$1.json