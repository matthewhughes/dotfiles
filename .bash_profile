# Sourced by login shell
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ]; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ -d $HOME/.bash_profile.d ]
then
    for f in $HOME/.bash_profile.d/*.sh
    do
        source $f
    done
fi

# startx
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
      exec startx
fi
