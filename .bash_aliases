conflict_regex='^(?:((>|<){7}\s\S+)|(={7}))$'
alias show_conflicts="ack '$conflict_regex'"
alias list_conflicts="ack -l '$conflict_regex'"

alias url_decode="perl -MURI::Escape -E 'say uri_unescape(\$ARGV[0]);'"
alias stupid_text="perl -E 'print rand() > 0.5 ? lc(\$_) : uc(\$_) foreach split(//, \$ARGV[0]); say;'"

alias xclipb="xclip -selection 'clipboard'"
alias ssh='TERM=xterm-color ssh'

function historvi {
    fc -re 'vim -S ~/.fcrc' -$HISTSIZE -1
}

function align_docs {
    perl -pe '$_ =~ s/(?:\s+|(\w+)):(?:\s+|(\w+))/$1 : $2/' | align
}
function mcd {
    mkdir -p "$1" && cd "$1"
}

# Enable Vi navigation in node REPL
alias node="NODE_NO_READLINE=1 rlwrap node"

alias kb_us='setxkbmap -layout us'
alias kb_intl='setxkbmap -layout us -variant intl'

alias build_pr="git log --reverse --format='- %s%n%n%b' master... \
    | grep -v '^Ticket:' \
    | sed 's/^[^-]/    &/g' \
    | cat --squeeze-blank"
